# Hoteles

## Diseño

Para estructurar la app se usó VIPER (que es una implementación de CLEAN) con una [modificación](http://khanlou.com/2015/01/the-coordinator/) para que los módulos no se conozcan entre sí. Básicamente consiste en compartir un Router entre varios módulos en vez de tener uno por módulo, delegando en este objeto las resposabilidades de navegación de todo un flujo (conjunto de módulos). De esta forma el Coordinator cumple las funciones de un Mediator, evitando que los módulos VIPER se referencien directamente unos a otros.

Las dependencias entre los componentes se manejan con inyección de dependencias. Cada módulo VIPER define sus dependencias, que se registran en el contenedor del Coordinator correspondiente.

## Bonus

- La opción de ver en un mapa la ubicación del hotel
- Uso de un caché en disco para que la app se pueda usar de forma offline (una vez que ya se cargaron los datos)

## Librerías externas

### Inyección de dependencias

- Swinject

### Eventos

- RxSwift

### Networking

- Alamofire
- Moya: para implementar un cliente de la API Rest de hoteles
- Kingfisher: para descargar, procesar y cachear las imágenes

### Vistas

- SkeletonView: para mostrar animaciones mientras se cargan los dat

## Correr la app

1. Descomprimir el zip o clonar el repo ubicado en <https://gitlab.com/Widcket/hoteles>.
2. Correr `pod install`
3. Abrir en Xcode y cambiar el signing
4. Compilar la app

## Dispositivos de prueba

La app se probó en un iPhone 6S Plus.
