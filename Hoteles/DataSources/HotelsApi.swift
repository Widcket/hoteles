//
//  HotelsApiDataSource.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import Moya

enum HotelsApi {
    
    case hotels(refresh: Bool)
    case hotel(id: String, refresh: Bool)
    
}

extension HotelsApi: TargetType {
    
    var baseURL: URL { return URL(string: Config.hotelsApiUrl)! }
    var method: Moya.Method { return .get }
    var headers: [String : String]? { return ["Accept": "application/json"] }
    var parameterEncoding: ParameterEncoding { return URLEncoding.default }
    var sampleData: Data { return Data() }
    
    var path: String {
        switch self {
        case .hotels(_): return "/hotels"
        case .hotel(let id, _): return "/hotels/\(id)"
        }
    }
    
    var task: Task {
        switch self {
        case .hotels(_): fallthrough
        case .hotel(_, _): return .requestParameters(parameters: [:], encoding: URLEncoding.default)
        }
    }
    
}

extension HotelsApi: Cachable {
    
    var cachePolicy: URLRequest.CachePolicy {
        switch self {
        case .hotels(let refresh): return refresh ? .useProtocolCachePolicy : .returnCacheDataElseLoad
        case .hotel(_, let refresh): return refresh ? .useProtocolCachePolicy : .returnCacheDataElseLoad
        }
    }
        
}
