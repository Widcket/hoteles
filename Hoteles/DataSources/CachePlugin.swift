//
//  CachePlugin.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 15/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import Moya

// From https://github.com/Moya/Moya/issues/976#issuecomment-434177611

protocol Cachable {
    
    var cachePolicy: URLRequest.CachePolicy { get }
    
}

final class CachePlugin: PluginType {
    
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        if let cacheableTarget = target as? Cachable {
            var mutableRequest = request
            
            mutableRequest.cachePolicy = cacheableTarget.cachePolicy
            
            return mutableRequest
        }
        
        return request
    }
    
}
