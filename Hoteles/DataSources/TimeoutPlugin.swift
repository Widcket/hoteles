//
//  TimeoutPlugin.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 15/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import Moya

final class TimeoutPlugin: PluginType {
    
    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        var mutableRequest = request
        
        mutableRequest.timeoutInterval = Config.networkTimeout
        
        return mutableRequest
    }
    
}
