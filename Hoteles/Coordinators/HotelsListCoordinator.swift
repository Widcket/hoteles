//
//  HotelsListCoordinator.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import Swinject

class HotelsListCoordinator: BaseCoordinator {
    
    func getRootViewController() -> UIViewController {
        return UINavigationController(rootViewController: HotelListScene.build(with: container))
    }
    
    override func registerDependencies() {
        container.register(HotelsListRouter.self, factory: { resolver in self })
        container.register(HotelDetailRouter.self, factory: { resolver in self })
        container.register(LightboxRouter.self, factory: { resolver in self })
        
        HotelListScene.registerDependencies(in: container)
        HotelDetailScene.registerDependencies(in: container)
        HotelReviewsScene.registerDependencies(in: container)
        LightboxScene.registerDependencies(in: container)
        MapScene.registerDependencies(in: container)
    }
    
}

extension HotelsListCoordinator: HotelsListRouter {
    
    func presentHotelDetails(from view: HotelsListView, for hotel: Hotel) {
        guard let view = view as? UIViewController else { return }
        
        let hotelDetailsViewController = HotelDetailScene.build(with: container, hotel: hotel)
                
        view.navigationController?.pushViewController(hotelDetailsViewController, animated: true)
    }
    
}

extension HotelsListCoordinator: HotelDetailRouter {
    
    func presentHotelPhoto(from view: HotelDetailView, for imageUrl: URL) {
        guard let view = view as? UIViewController else { return }
        
        let lightboxViewController = LightboxScene.build(with: container, imageUrl: imageUrl)
        
        lightboxViewController.modalPresentationStyle = .overCurrentContext
        
        view.present(lightboxViewController, animated: true)
    }
    
    func presentHotelReviews(from view: HotelDetailView, for reviews: [Review]) {
        guard let view = view as? UIViewController else { return }
        
        let hotelReviewsViewController = HotelReviewsScene.build(with: container, reviews: reviews)
        
        view.navigationController?.pushViewController(hotelReviewsViewController, animated: true)
    }
    
    func presentHotelLocation(from view: HotelDetailView, for location: GeoLocation) {
        guard let view = view as? UIViewController else { return }
        
        let mapViewController = MapScene.build(with: container, location: location)
        
        view.navigationController?.pushViewController(mapViewController, animated: true)
    }
    
}

extension HotelsListCoordinator: LightboxRouter {
    
    func dismissLightbox(from view: LightboxView) {
        guard let view = view as? UIViewController else { return }
        
        view.dismiss(animated: true)
    }
    
}
