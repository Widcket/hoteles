//
//  BaseTableViewController.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 15/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit

class BaseTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
    }
    
    func setupTableView() {
        tableView.tableFooterView = UIView(frame: .zero)
    }
    
}
