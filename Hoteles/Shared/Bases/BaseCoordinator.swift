//
//  BaseCoordinator.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import Swinject

class BaseCoordinator {
    
    let container: Container
    
    init(parentContainer: Container) {
        self.container = Container(parent: parentContainer, defaultObjectScope: .container)
        
        registerDependencies()
    }
    
    func registerDependencies() {}
    
}
