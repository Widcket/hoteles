//
//  ErrorView.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 15/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit

class DataErrorView: UIView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    // MARK: - Setup method
    
    private func setupView() {
        UINib(nibName: DataErrorView.defaultXib, bundle: .main).instantiate(withOwner: self, options: nil)
        
        addSubview(containerView)
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints([
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        titleLabel.text = "DataError_title".localized
        messageLabel.text = "DataError_message".localized
    }
    
}
