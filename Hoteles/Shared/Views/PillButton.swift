//
//  PillButton.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit

class PillButton: UIButton {
    
    // From https://blog.usejournal.com/custom-uiview-in-swift-done-right-ddfe2c3080a
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    // MARK: - Setup methods

    private func setupView() {
        let verticalPadding: CGFloat = 10
        let horizontalPadding: CGFloat = verticalPadding * 2
        
        contentEdgeInsets = UIEdgeInsets(top: verticalPadding,
                                         left: horizontalPadding,
                                         bottom: verticalPadding,
                                         right: horizontalPadding)
        
        layer.cornerRadius = 15
        layer.borderColor = tintColor.cgColor
        layer.borderWidth = 0.5
        tintColor = .black
        titleLabel?.textColor = tintColor
    }
    
}
