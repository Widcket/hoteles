//
//  City.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct City: Codable {
    
    let id: String
    let code: String
    let name: String
    let country: CityCountry
    let administrativeDivision: CityAdministrativeDivision
    
    private enum CodingKeys: String, CodingKey {
        
        case id, code, name, country
        case administrativeDivision = "administrative_division"
        
    }
    
}
