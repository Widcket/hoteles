//
//  CityAdministrativeDivision.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct CityAdministrativeDivision: Codable {
    
    let id: String
    let code: String
    let name: String
    
}
