//
//  ReviewUser.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct ReviewUser: Codable {
    
    let name: String
    let firstName: String?
    let lastName: String?
    let country: String
    
    private enum CodingKeys: String, CodingKey {
        
        case name, country
        case firstName = "first_name"
        case lastName = "last_name"
        
    }
    
}
