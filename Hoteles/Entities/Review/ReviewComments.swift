//
//  ReviewComments.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct ReviewComments: Codable {
    
    var title: String?
    var good: String?
    var bad: String?
    var type: String
    
}

