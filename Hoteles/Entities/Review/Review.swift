//
//  Review.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct Review: Codable {
    
    let comments: ReviewComments
    let user: ReviewUser
    
}
