//
//  Hotel.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct Hotel: Codable {
    
    let id: String
    let description: String?
    let stars: Int
    let name: String
    let address: String
    var mainPicture: URL?
    let rating: Float
    let amenities: [Amenity]
    var price: Price?
    var geoLocation: GeoLocation?
    var city: City?
    var reviews: [Review]?
    
    private enum CodingKeys: String, CodingKey {
        
        case id, description, stars, name, address, rating, amenities, price, city, reviews
        case mainPicture = "main_picture"
        case geoLocation = "geo_location"
        
    }
    
}
