//
//  Amenity.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct Amenity: Codable {
    
    let type: AmenityType
    let description: String
    
    private enum CodingKeys: String, CodingKey {
        
        case description
        case type = "id"
        
    }
    
}
