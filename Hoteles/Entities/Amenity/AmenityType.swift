//
//  AmenityType.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

enum AmenityType: String, Codable {
    
    case breakfast = "BREAKFST"
    case wifi = "WIFI"
    case parking = "PARKING"
    case pool = "PISCN"
    
}
