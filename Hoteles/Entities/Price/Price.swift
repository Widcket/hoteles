//
//  Price.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct Price: Codable {
    
    let currency: PriceCurrency
    let finalPrice: Bool
    let base: Int
    let best: Int
    
    private enum CodingKeys: String, CodingKey {
        
        case currency, base, best
        case finalPrice = "final_price"
        
    }
    
}
