//
//  PriceCurrency.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

struct PriceCurrency: Codable {
    
    let code: String
    let mask: String
    let ratio: Double
    
}
