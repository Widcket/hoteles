//
//  FetchHotels.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import RxSwift

protocol HotelsListRepository {
    
    func fetchHotels(refresh: Bool) -> Single<[Hotel]>
    
}

final class FetchHotels {
    
    private var repository: HotelsListRepository
    
    init(repository: HotelsListRepository) {
        self.repository = repository
    }
    
    func execute(refresh: Bool = false) -> Single<[Hotel]> {
        return repository.fetchHotels(refresh: refresh)
    }
    
}
