//
//  FetchHotel.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import RxSwift

protocol HotelDetailRepository {
    
    func fetchHotel(id: String, refresh: Bool) -> Maybe<Hotel>
    
}

final class FetchHotel {
    
    private var repository: HotelDetailRepository
    
    init(repository: HotelDetailRepository) {
        self.repository = repository
    }
    
    func execute(id: String, refresh: Bool = false) -> Maybe<Hotel> {
        return repository.fetchHotel(id: id, refresh: refresh).catchError({ _ in Maybe.empty() })
    }
    
}
