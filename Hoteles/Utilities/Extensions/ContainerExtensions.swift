//
//  ContainerExtensions.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import Swinject

extension Container {
    
    func registerIfUnregistered<Service>(_ serviceType: Service.Type,
                                         factory: @escaping (Resolver) -> Service) {
        let loggingFunction = Container.loggingFunction
        
        // We want to register a dependency only if it wasn't already registered
        // But attempting to resolve the dependency to do the check will log an error if it wasn't registered
        // So we disable logging temporarily
        Container.loggingFunction = nil
        
        if resolve(serviceType) == nil {
            register(serviceType, factory: factory)
        }
        
        Container.loggingFunction = loggingFunction
    }
    
}
