//
//  StringExtensions.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

extension String {
    
    // From https://medium.com/@tprudat/ive-extended-string-to-allow-a-less-painful-way-to-use-nslocalizedstring-d584c9a8a26c
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
}
