//
//  Reusable.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit

protocol Reusable: Configurable {
    
    static var defaultIdentifier: String { get }
    
}

extension Reusable where Self: UITableViewCell {
    
    static var defaultIdentifier: String {
        return String(describing: self)
    }
    
}
