//
//  Configurable.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

// from https://medium.com/chili-labs/configuring-multiple-cells-with-generics-in-swift-dcd5e209ba16

protocol Configurable: class {
    
    associatedtype D
    
    func configure(with data: D)
    
}
