//
//  XibLoadable.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 15/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit

protocol XibDesignable: class {
    
    static var defaultXib: String { get }
    
}

extension XibDesignable where Self: UIView {
    
    static var defaultXib: String {
        return String(describing: self)
    }

}

extension UIView: XibDesignable {}
