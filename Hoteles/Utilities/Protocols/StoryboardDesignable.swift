//
//  StoryboardDesignable.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

// From https://codeburst.io/simpler-ios-storyboard-instantiation-97ca4bfb63bd

import Foundation
import UIKit

protocol StoryboardDesignable: class {

    static var defaultStoryboard: String { get }
    static var defaultIdentifier: String { get }

}

extension StoryboardDesignable where Self: UIViewController {

    static var defaultStoryboard: String {
        return "\(defaultIdentifier)View"
    }

    static var defaultIdentifier: String {
        var viewControllerName = String(describing: self)
        let suffix = "ViewController"

        if viewControllerName.hasSuffix(suffix) {
            viewControllerName.removeLast(suffix.count)
        }

        return viewControllerName
    }

    static func fromStoryboard(in bundle: Bundle = Bundle.main) -> Self {
        let storyboard = UIStoryboard(name: defaultStoryboard, bundle: nil)

        guard let instance = storyboard.instantiateInitialViewController() as? Self else {
            fatalError("Could not instantiate view controller \(defaultIdentifier)")
        }

        return instance
    }

}

extension UIViewController: StoryboardDesignable {}
