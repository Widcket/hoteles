//
//  Config.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import Alamofire

final class Config {
    
    private init() {}
    
    static let hotelsApiUrl = "https://private-a2ba2-jovenesdealtovuelo.apiary-mock.com/"
    static let networkTimeout = TimeInterval(10)
    
}
