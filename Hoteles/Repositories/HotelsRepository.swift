//
//  HotelsRepository.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import Moya
import RxSwift

fileprivate enum HotelKeyPath: String {
    
    case hotels = "items"
    case hotel = "hotel"
    case price = "price"
    
}

final class HotelsRepository {
    
    private let api: MoyaProvider<HotelsApi>
    private let retries = 3
    
    init(api: MoyaProvider<HotelsApi>) {
        self.api = api
    }
    
    public func fetchHotels(refresh: Bool = false) -> Single<[Hotel]> {
        return api.rx
            .request(HotelsApi.hotels(refresh: refresh))
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .retry(retries)
            .filterSuccessfulStatusCodes()
            .map([Hotel].self, atKeyPath: HotelKeyPath.hotels.rawValue)
    }
    
    public func fetchHotel(id: String, refresh: Bool = false) -> Maybe<Hotel> {
        return api.rx
            .request(HotelsApi.hotel(id: id, refresh: refresh))
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .retry(retries)
            .filterSuccessfulStatusCodes()
            .asMaybe()
            .flatMap { response in
                let price = try response.map(Price.self, atKeyPath: HotelKeyPath.price.rawValue)
                var hotel = try response.map(Hotel.self, atKeyPath: HotelKeyPath.hotel.rawValue)
                
                hotel.price = price
                
                return Maybe.just(hotel)
            }
    }
    
}

extension HotelsRepository: HotelsListRepository {}

extension HotelsRepository: HotelDetailRepository {}
