//
//  HotelReviewsScene.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import Swinject

// MARK: - Scene protocols

protocol HotelReviewsView: class {
    
    var eventsHandler: HotelReviewsEventsHandler? { get set }
    
    func displayReviews(_ reviews: [Review])
    
}

protocol HotelReviewsEventsHandler: class {
    
    var view: HotelReviewsView? { get set }
    var reviews: [Review] { get set }
    
    func viewWillAppear()
    
}

// MARK: - Scene

final class HotelReviewsScene {
    
    private init() {}
    
    static func build(with container: Container, reviews: [Review]) -> UIViewController {
        guard let view = container.resolve(HotelReviewsView.self),
            let presenter = container.resolve(HotelReviewsEventsHandler.self) else {
                fatalError("Unable to resolve \(String(describing: self)) dependencies")
        }
        
        view.eventsHandler = presenter
        presenter.view = view
        presenter.reviews = reviews
        
        return view as! UIViewController
    }
    
    static func registerDependencies(in container: Container) {
        container.register(HotelReviewsView.self) { resolver in
            return HotelReviewsViewController.fromStoryboard()
        }
        
        container.register(HotelReviewsEventsHandler.self) { resolver in
            return HotelReviewsPresenter()
        }
    }
    
}
