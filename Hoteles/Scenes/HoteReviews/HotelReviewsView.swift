//
//  HotelReviewsView.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit

final class HotelReviewsViewController: BaseTableViewController {

    weak var eventsHandler: HotelReviewsEventsHandler?

    fileprivate var reviews: [Review] = []
    
    // MARK: - Overrides

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTitle()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        eventsHandler?.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        scrollToTop()
    }
    
    override func setupTableView() {
        super.setupTableView()
        
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 100
    }
    
    // MARK: - Setup methods

    private func setupTitle() {
        title = "HotelReviews_title".localized
    }
    
    // MARK: - View manipulation methods

    private func scrollToTop() {
        tableView.scrollRectToVisible(CGRect(x: 0, y: 1, width: 1, height: 1), animated: false)
    }

}

extension HotelReviewsViewController: HotelReviewsView {

    func displayReviews(_ reviews: [Review]) {
        self.reviews = reviews
        
        tableView.reloadData()
    }

}

extension HotelReviewsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = HotelReviewCell.defaultIdentifier
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! HotelReviewCell

        cell.configure(with: reviews[indexPath.row])

        return cell
    }

}
