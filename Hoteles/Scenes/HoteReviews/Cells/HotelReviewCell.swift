//
//  HotelReviewCell.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit

class HotelReviewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thumbsUpContainerView: UIView!
    @IBOutlet weak var thumbsUpImageView: UIImageView!
    @IBOutlet weak var thumbsUpLabel: UILabel!
    @IBOutlet weak var thumbsDownContainerView: UIView!
    @IBOutlet weak var thumbsDownImageView: UIImageView!
    @IBOutlet weak var thumbsDownLabel: UILabel!
    
    // MARK: - Overrides
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        thumbsUpImageView.tintColor = .lightGray
        thumbsDownImageView.tintColor = .lightGray
        
        prepareForReuse()
    }
    
    override func prepareForReuse() {
        titleLabel.isHidden = false
        thumbsUpContainerView.isHidden = false
        thumbsDownContainerView.isHidden = false
        
        nameLabel.text = nil
        titleLabel.text = nil
        thumbsUpLabel.text = nil
        thumbsDownLabel.text = nil
    }
    
}

extension HotelReviewCell: Reusable {
    
    func configure(with data: Review) {
        nameLabel.text = data.user.name
        
        if let title = data.comments.title {
            titleLabel.text = title
        }
        else {
            titleLabel.isHidden = true
        }
        
        if let thumbsUpComment = data.comments.good {
            thumbsUpLabel.text = thumbsUpComment
        }
        else {
            thumbsUpContainerView.isHidden = true
        }
        
        if let thumbsDownComment = data.comments.bad {
            thumbsDownLabel.text = thumbsDownComment
        }
        else {
            thumbsDownContainerView.isHidden = true
        }
    }
    
}
