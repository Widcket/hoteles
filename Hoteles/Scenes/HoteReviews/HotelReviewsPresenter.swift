//
//  HotelReviewsPresenter.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

final class HotelReviewsPresenter {
    
    var view: HotelReviewsView?
    var reviews: [Review] = []
    
}

extension HotelReviewsPresenter: HotelReviewsEventsHandler {
    
    func viewWillAppear() {
        view?.displayReviews(reviews)
    }
    
}
