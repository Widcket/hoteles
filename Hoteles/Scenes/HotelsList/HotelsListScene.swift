//
//  HotelsListScene.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import Moya

// MARK: - Scene protocols

protocol HotelsListView: class {
    
    var eventsHandler: HotelsListEventsHandler? { get set }
    
    func displayHotels(_ hotels: [Hotel])
    func displayError()
    func stopRefreshing()
    
}

protocol HotelsListEventsHandler: class {
    
    var router: HotelsListRouter? { get set }
    var view: HotelsListView? { get set }
    
    func viewDidLoad()
    func didStartRefreshing()
    func didSelectHotel(_ hotel: Hotel)
    
}

protocol HotelsListRouter: class {
    
    func presentHotelDetails(from view: HotelsListView, for hotel: Hotel)
    
}

// MARK: - Scene

final class HotelListScene {
    
    private init() {}
    
    static func build(with container: Container) -> UIViewController {
        guard let view = container.resolve(HotelsListView.self),
            let presenter = container.resolve(HotelsListEventsHandler.self),
            let router = container.resolve(HotelsListRouter.self) else {
                fatalError("Unable to resolve \(String(describing: self)) dependencies")
        }
        
        view.eventsHandler = presenter
        presenter.view = view
        presenter.router = router
        
        return view as! UIViewController
    }
    
    static func registerDependencies(in container: Container) {
        container.registerIfUnregistered(MoyaProvider<HotelsApi>.self) { resolver in
            return MoyaProvider<HotelsApi>(plugins: [CachePlugin(), TimeoutPlugin()])
        }
        
        container.registerIfUnregistered(HotelsListRepository.self) { resolver in
            return HotelsRepository(api: resolver.resolve(MoyaProvider<HotelsApi>.self)!)
        }
        
        container.registerIfUnregistered(FetchHotels.self) { resolver in
            return FetchHotels(repository: resolver.resolve(HotelsListRepository.self)!)
        }
        
        container.register(HotelsListView.self) { resolver in
            return HotelsListViewController.fromStoryboard()
        }
        
        container.register(HotelsListEventsHandler.self) { resolver in
            return HotelsListPresenter(hotelsFetcher: resolver.resolve(FetchHotels.self)!)
        }
    }
    
}
