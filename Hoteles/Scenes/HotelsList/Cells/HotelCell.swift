//
//  HotelCell.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class HotelCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var topPaddingConstraint: NSLayoutConstraint!
    @IBOutlet var leftPaddingConstraint: NSLayoutConstraint!
    
    private var cornerRadius: CGFloat = 10
    
    // MARK: - Overrides

    override func awakeFromNib() {
        super.awakeFromNib()

        setupView()
        prepareForReuse()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupShadow()
    }
    
    override func prepareForReuse() {
        photoImageView.kf.cancelDownloadTask()

        photoImageView.image = #imageLiteral(resourceName: "defaultPhoto.pdf")
        nameLabel.text = nil
    }
    
    // MARK: - Setup methods
    
    private func setupView() {
        backgroundColor = .clear
        selectionStyle = .none
        containerView.backgroundColor = .white
        containerView.layer.cornerRadius = cornerRadius
        containerView.layer.borderColor = UIColor.lightGray.cgColor
        containerView.layer.borderWidth = 0.5
    }
    
    private func setupShadow() {
        let insetBounds = bounds.insetBy(dx: leftPaddingConstraint.constant, dy: topPaddingConstraint.constant)
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowPath = UIBezierPath(roundedRect: insetBounds, cornerRadius: cornerRadius).cgPath
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0.22
        layer.shadowRadius = 4
    }

}

extension HotelCell: Reusable {

    func configure(with data: Hotel) {
        nameLabel.text = data.name

        if let photoUrl = data.mainPicture {
            photoImageView.kf.indicatorType = .activity
            
            photoImageView.kf.setImage(with: photoUrl,
                                       placeholder: #imageLiteral(resourceName: "defaultPhoto.pdf"),
                                       options: [.backgroundDecode, .transition(.fade(1))])
        }
    }

}
