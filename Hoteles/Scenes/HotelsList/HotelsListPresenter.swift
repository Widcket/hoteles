//
//  HotelsListPresenter.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import RxSwift

final class HotelsListPresenter {
        
    weak var router: HotelsListRouter?
    var view: HotelsListView?
    
    private let hotelsFetcher: FetchHotels
    private let disposeBag: DisposeBag = DisposeBag()
    
    init(hotelsFetcher: FetchHotels) {
        self.hotelsFetcher = hotelsFetcher
    }
    
    func fetchHotels(isRefreshing: Bool = false) {
        hotelsFetcher.execute(refresh: isRefreshing)
            .observeOn(MainScheduler.instance)
            .do(onSuccess: { [weak self] hotels in
                self?.view?.displayHotels(hotels)
                
                if isRefreshing {
                    self?.view?.stopRefreshing()
                }
            }, onError: { [weak self] _ in
                self?.view?.displayError()
                
                if isRefreshing {
                    self?.view?.stopRefreshing()
                }
            })
            .subscribe()
            .disposed(by: disposeBag)
    }
    
}

extension HotelsListPresenter: HotelsListEventsHandler {
    
    func viewDidLoad() {
        fetchHotels()
    }
    
    func didStartRefreshing() {
        fetchHotels(isRefreshing: true)
    }
    
    func didSelectHotel(_ hotel: Hotel) {
        guard let view = view else { return }
        
        router?.presentHotelDetails(from: view, for: hotel)
    }
    
}
