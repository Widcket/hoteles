//
//  HotelsListView.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import SkeletonView

final class HotelsListViewController: BaseTableViewController {
    
    weak var eventsHandler: HotelsListEventsHandler?

    fileprivate let refreshControl = UIRefreshControl()
    fileprivate var hotels: [Hotel] = []
    fileprivate let estimatedRowHeight: CGFloat = 250
    fileprivate let skeletonRows = 5
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTitle()
        setupRefreshControl()
        showSkeletonView()
        eventsHandler?.viewDidLoad()
    }

    override func setupTableView() {
        super.setupTableView()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = estimatedRowHeight
        tableView.refreshControl = refreshControl
    }
    
    // MARK: - Setup methods
    
    private func setupTitle() {
        title = Bundle.main.displayName
    }

    private func setupRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshHotels), for: .valueChanged)
    }
    
    // MARK: - View manipulation methods

    private func showSkeletonView() {
        view.showAnimatedGradientSkeleton()
    }

    private func hideSkeletonView() {
        guard view.isSkeletonActive else { return }
        
        view.hideSkeleton()

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = estimatedRowHeight
    }
    
    private func hideErrorView() {
        guard tableView.backgroundView != nil else { return }
        
        tableView.backgroundView = nil
    }
    
    // MARK: - Target methods
    
    @objc private func refreshHotels() {
        eventsHandler?.didStartRefreshing()
    }

}

extension HotelsListViewController: HotelsListView {
    
    func displayHotels(_ hotels: [Hotel]) {
        self.hotels = hotels
        
        hideErrorView()
        hideSkeletonView()
        tableView.reloadData()
    }
    
    func displayError() {
        guard hotels.isEmpty else { return }
        
        hideSkeletonView()
        
        tableView.backgroundView = DataErrorView()
        
        if !refreshControl.isRefreshing {
            tableView.reloadData()
        }
    }
    
    func stopRefreshing() {
        refreshControl.endRefreshing()
        tableView.reloadData()
    }

}

extension HotelsListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: HotelCell.defaultIdentifier, for: indexPath) as! HotelCell

        cell.hideSkeleton()
        cell.configure(with: hotels[indexPath.row])

        return cell
    }

}

extension HotelsListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        eventsHandler?.didSelectHotel(hotels[indexPath.row])
    }

}

extension HotelsListViewController: SkeletonTableViewDataSource {

    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return skeletonRows
    }

    func collectionSkeletonView(_ skeletonView: UITableView,
                                cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return HotelCell.defaultIdentifier
    }

}
