//
//  LightboxView.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

final class LightboxViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBAction func didTapCloseButton() {
        eventsHandler?.didTapCloseButton()
    }
    
    weak var eventsHandler: LightboxEventsHandler?
    
    // MARK: - Overrides
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        eventsHandler?.viewWillAppear()
    }
    
}

extension LightboxViewController: LightboxView {
    
    func displayImage(from url: URL) {
        imageView.kf.indicatorType = .activity
        
        imageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "defaultPhoto.pdf"), options: [.backgroundDecode, .transition(.fade(1))])
    }
    
}
