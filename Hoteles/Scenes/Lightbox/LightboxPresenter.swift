//
//  LightboxPresenter.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

final class LightboxPresenter {
    
    weak var router: LightboxRouter?
    var view: LightboxView?
    
    var imageUrl: URL?
    
}

extension LightboxPresenter: LightboxEventsHandler {

    func viewWillAppear() {
        guard let view = view, let imageUrl = imageUrl else { return }
        
        view.displayImage(from: imageUrl)
    }
    
    func didTapCloseButton() {
        guard let view = view else { return }
        
        router?.dismissLightbox(from: view)
    }
    
}
