//
//  LightboxScene.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import Swinject

// MARK: - Scene protocols

protocol LightboxView: class {
    
    var eventsHandler: LightboxEventsHandler? { get set }
    
    func displayImage(from url: URL)
    
}

protocol LightboxEventsHandler: class {
    
    var router: LightboxRouter? { get set }
    var view: LightboxView? { get set }
    var imageUrl: URL? { get set }
    
    func viewWillAppear()
    func didTapCloseButton()
    
}

protocol LightboxRouter: class {
    
    func dismissLightbox(from view: LightboxView)
    
}

// MARK: - Scene

final class LightboxScene {
    
    private init() {}
    
    static func build(with container: Container, imageUrl: URL) -> UIViewController {
        guard let view = container.resolve(LightboxView.self),
            let presenter = container.resolve(LightboxEventsHandler.self),
            let router = container.resolve(LightboxRouter.self) else {
                fatalError("Unable to resolve \(String(describing: self)) dependencies")
        }
        
        view.eventsHandler = presenter
        presenter.view = view
        presenter.router = router
        presenter.imageUrl = imageUrl
        
        return view as! UIViewController
    }
    
    static func registerDependencies(in container: Container) {
        
        container.register(LightboxView.self) { resolver in
            return LightboxViewController.fromStoryboard()
        }
        
        container.register(LightboxEventsHandler.self) { resolver in
            return LightboxPresenter()
        }
    }
    
}
