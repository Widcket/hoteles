//
//  HotelDetailPresenter.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import RxSwift

final class HotelDetailPresenter {
    
    weak var router: HotelDetailRouter?
    var view: HotelDetailView?
    var hotel: Hotel?
    
    private let hotelFetcher: FetchHotel
    private let disposeBag: DisposeBag = DisposeBag()
    
    init(hotelFetcher: FetchHotel) {
        self.hotelFetcher = hotelFetcher
    }
    
    func fetchHotelDetail(id: String, isRefreshing: Bool = false) {
        hotelFetcher.execute(id: id, refresh: isRefreshing)
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] hotel in
                self?.hotel = hotel
                
                self?.view?.displayHotel(hotel)
                
                if isRefreshing {
                    self?.view?.stopRefreshing()
                }
            }, onCompleted: { [weak self] in
                if isRefreshing {
                    self?.view?.stopRefreshing()
                }
            })
            .disposed(by: disposeBag)
    }
    
}

extension HotelDetailPresenter: HotelDetailEventsHandler {

    func viewWillAppear() {
        guard let hotel = hotel else { return }
        
        view?.displayHotel(hotel)
        fetchHotelDetail(id: hotel.id)
    }
    
    func didStartRefreshing() {
        guard let hotel = hotel else { return }
        
        fetchHotelDetail(id: hotel.id, isRefreshing: true)
    }
    
    func didTapHotelPhoto(_ imageUrl: URL) {
        guard let view = view else { return }
        
        router?.presentHotelPhoto(from: view, for: imageUrl)
    }
    
    func didTapHotelReviewsButton() {
        guard let view = view, let reviews = hotel?.reviews else { return }
        
        router?.presentHotelReviews(from: view, for: reviews)
    }
    
    func didTapHotelLocationButton() {
        guard let view = view, let location = hotel?.geoLocation else { return }
        
        router?.presentHotelLocation(from: view, for: location)
    }
    
}
