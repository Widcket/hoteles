//
//  HotelPhotoCell.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class HotelPhotoCell: UITableViewCell {

    @IBOutlet weak var photoImageView: UIImageView!
    
    // MARK: - Overrides

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

        prepareForReuse()
    }

    override func prepareForReuse() {
        photoImageView.image = #imageLiteral(resourceName: "defaultPhoto.pdf")
    }

}

extension HotelPhotoCell: Reusable {

    func configure(with data: URL?) {
        guard let photoUrl = data else { return }
        
        photoImageView.kf.indicatorType = .activity
        
        photoImageView.kf.setImage(with: photoUrl,
                                   placeholder: #imageLiteral(resourceName: "defaultPhoto.pdf"),
                                   options: [.backgroundDecode, .transition(.fade(1))])
    }

}
