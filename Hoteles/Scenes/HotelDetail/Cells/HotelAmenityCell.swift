//
//  HotelAmenityCell.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit

class HotelAmenityCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var amenityLabel: UILabel!
    
    // MARK: - Overrides
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        iconImageView.tintColor = .lightGray
        
        prepareForReuse()
    }
    
    override func prepareForReuse() {
        iconImageView.image = nil
        amenityLabel.text = nil
    }
    
}

extension HotelAmenityCell: Reusable {
    
    func configure(with data: Amenity) {
        switch data.type {
        case .breakfast: iconImageView.image = #imageLiteral(resourceName: "breakfast.pdf")
        case .parking: iconImageView.image = #imageLiteral(resourceName: "parking.pdf")
        case .pool: iconImageView.image = #imageLiteral(resourceName: "pool.pdf")
        case .wifi: iconImageView.image = #imageLiteral(resourceName: "wifi.pdf")
        }
        
        amenityLabel.text = data.description
    }
    
}
