//
//  HotelDetailCell.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit

protocol HotelDetailCellDelegate: class {

    func didTapShowLocationButton()
    func didTapShowReviewsButton()

}

class HotelDetailCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var starsImageView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var showLocationButton: UIButton!
    @IBOutlet weak var showReviewsButton: UIButton!

    @IBAction func didTapShowLocationButton() {
        delegate?.didTapShowLocationButton()
    }

    @IBAction func didTapShowReviewsButton() {
        delegate?.didTapShowReviewsButton()
    }

    weak var delegate: HotelDetailCellDelegate?
    
    // MARK: - Overrides

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none
        starsImageView.tintColor = .lightGray
        
        showLocationButton.setTitle("HotelDetail_hotelLocation".localized.uppercased(), for: .normal)
        showReviewsButton.setTitle("HotelDetail_hotelReviews".localized.uppercased(), for: .normal)
        
        prepareForReuse()
    }

    override func prepareForReuse() {
        delegate = nil
        showReviewsButton.isHidden = false
        nameLabel.text = nil
        starsLabel.text = nil
        addressLabel.text = nil
    }

}

extension HotelDetailCell: Reusable {

    func configure(with data: Hotel) {
        if data.reviews == nil {
            showReviewsButton.isHidden = true
        }
        
        nameLabel.text = data.name
        starsLabel.text = "\(data.stars)"
        addressLabel.text = data.address
    }

}
