//
//  HotelDescriptionCell.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit

class HotelDescriptionCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - Overrides
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        
        prepareForReuse()
    }
    
    override func prepareForReuse() {
        descriptionLabel.text = nil
    }
    
}

extension HotelDescriptionCell: Reusable {
    
    func configure(with data: String?) {
        descriptionLabel.text = data
    }
    
}
