//
//  HotelDetailView.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import SkeletonView

fileprivate enum DetailSection: Int, CaseIterable {
    case photo, info, amenities, description
}

final class HotelDetailViewController: BaseTableViewController {

    weak var eventsHandler: HotelDetailEventsHandler?

    fileprivate var hotel: Hotel?
    fileprivate let refreshControl = UIRefreshControl()
    fileprivate let estimatedRowHeight: CGFloat = 150
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRefreshControl()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

        eventsHandler?.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        scrollToTop()
    }

    override func setupTableView() {
        super.setupTableView()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = estimatedRowHeight
        tableView.refreshControl = refreshControl
    }
    
    // MARK: - Setup methods
    
    private func setupRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshHotelDetail), for: .valueChanged)
    }
    
    // MARK: - Targets
    
    @objc private func refreshHotelDetail() {
        eventsHandler?.didStartRefreshing()
    }
    
    // MARK: - View manipulation methods
    
    private func scrollToTop() {
        tableView.setContentOffset(.zero, animated: false)
    }

}

extension HotelDetailViewController: HotelDetailView {

    func displayHotel(_ hotel: Hotel) {
        self.hotel = hotel
        
        if !refreshControl.isRefreshing {
            tableView.reloadData()
        }
    }
    
    func stopRefreshing() {
        refreshControl.endRefreshing()
    }

}

extension HotelDetailViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        if hotel == nil { return 0 }

        return DetailSection.allCases.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let hotel = hotel, let section = DetailSection(rawValue: section) else {
            return 0
        }

        switch section {
        case .photo, .info, .description: return 1
        case .amenities: return hotel.amenities.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let hotel = self.hotel,
            let section = DetailSection(rawValue: indexPath.section) else {
                return UITableViewCell()
        }

        switch section {
        case .photo:
            let identifier = HotelPhotoCell.defaultIdentifier
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! HotelPhotoCell

            cell.configure(with: hotel.mainPicture)

            return cell
        case .info:
            let identifier = HotelDetailCell.defaultIdentifier
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! HotelDetailCell

            cell.configure(with: hotel)
            cell.delegate = self

            return cell
        case .amenities:
            let identifier = HotelAmenityCell.defaultIdentifier
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! HotelAmenityCell

            cell.configure(with: hotel.amenities[indexPath.row])

            return cell
        case .description:
            let identifier = HotelDescriptionCell.defaultIdentifier
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! HotelDescriptionCell

            cell.configure(with: hotel.description)

            return cell
        }
    }

}

extension HotelDetailViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == DetailSection.photo.rawValue, let imageUrl = hotel?.mainPicture {
            eventsHandler?.didTapHotelPhoto(imageUrl)
        }
    }

}

extension HotelDetailViewController: HotelDetailCellDelegate {

    func didTapShowLocationButton() {
        eventsHandler?.didTapHotelLocationButton()
    }

    func didTapShowReviewsButton() {
        eventsHandler?.didTapHotelReviewsButton()
    }

}

extension HotelDetailViewController: SkeletonTableViewDataSource {

    func numSections(in collectionSkeletonView: UITableView) -> Int {
        return DetailSection.allCases.count
    }

    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func collectionSkeletonView(_ skeletonView: UITableView,
                                cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        guard let section = DetailSection(rawValue: indexPath.section) else {
            return HotelPhotoCell.defaultIdentifier
        }

        switch section {
        case .photo: return HotelPhotoCell.defaultIdentifier
        case .info: return HotelDetailCell.defaultIdentifier
        case .amenities: return HotelAmenityCell.defaultIdentifier
        case .description: return HotelDescriptionCell.defaultIdentifier
        }
    }

}
