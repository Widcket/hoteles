//
//  HotelDetailScene.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 13/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import Moya

// MARK: - Scene protocols

protocol HotelDetailView: class {
    
    var eventsHandler: HotelDetailEventsHandler? { get set }
    
    func displayHotel(_ hotel: Hotel)
    func stopRefreshing()
    
}

protocol HotelDetailEventsHandler: class {
    
    var router: HotelDetailRouter? { get set }
    var view: HotelDetailView? { get set }
    var hotel: Hotel? { get set }
    
    func viewWillAppear()
    func didStartRefreshing()
    func didTapHotelPhoto(_ imageUrl: URL)
    func didTapHotelReviewsButton()
    func didTapHotelLocationButton()
    
}

protocol HotelDetailRouter: class {
    
    func presentHotelPhoto(from view: HotelDetailView, for imageUrl: URL)
    func presentHotelReviews(from view: HotelDetailView, for reviews: [Review])
    func presentHotelLocation(from view: HotelDetailView, for location: GeoLocation)
    
}

// MARK: - Scene

final class HotelDetailScene {
    
    private init() {}
    
    static func build(with container: Container, hotel: Hotel) -> UIViewController {
        guard let view = container.resolve(HotelDetailView.self),
            let presenter = container.resolve(HotelDetailEventsHandler.self),
            let router = container.resolve(HotelDetailRouter.self) else {
                fatalError("Unable to resolve \(String(describing: self)) dependencies")
        }
        
        view.eventsHandler = presenter
        presenter.view = view
        presenter.router = router
        presenter.hotel = hotel
        
        return view as! UIViewController
    }
    
    static func registerDependencies(in container: Container) {
        container.registerIfUnregistered(MoyaProvider<HotelsApi>.self) { resolver in
            return MoyaProvider<HotelsApi>(plugins: [CachePlugin(), TimeoutPlugin()])
        }
        
        container.registerIfUnregistered(HotelDetailRepository.self) { resolver in
            return HotelsRepository(api: resolver.resolve(MoyaProvider<HotelsApi>.self)!)
        }
        
        container.registerIfUnregistered(FetchHotel.self) { resolver in
            return FetchHotel(repository: resolver.resolve(HotelDetailRepository.self)!)
        }
        
        container.register(HotelDetailView.self) { resolver in
            return HotelDetailViewController.fromStoryboard()
        }
        
        container.register(HotelDetailEventsHandler.self) { resolver in
            return HotelDetailPresenter(hotelFetcher: resolver.resolve(FetchHotel.self)!)
        }
    }
    
}
