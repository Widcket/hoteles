//
//  MapView.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import MapKit

final class MapViewController: UIViewController {
    
    fileprivate weak var mapView: MKMapView?
    
    weak var eventsHandler: MapEventsHandler?
    
    // MARK: - Overrides
    
    override func viewDidLoad() {
        setupTitle()
        setupMap()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if mapView == nil {
            setupMap()
        }
        
        eventsHandler?.viewWillAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        releaseMap()
    }
    
    // MARK: - Setup methods
    
    private func setupTitle() {
        title = "Map_title".localized
    }
    
    private func setupMap() {
        let map = MKMapView()
        
        view.addSubview(map)
        
        map.translatesAutoresizingMaskIntoConstraints = false
        map.mapType = .standard
        map.isZoomEnabled = true
        
        let topConstraint = map.topAnchor.constraint(equalTo: view.topAnchor)
        let rightConstraint = map.rightAnchor.constraint(equalTo: view.rightAnchor)
        let bottomConstraint = map.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        let leftConstraint = map.leftAnchor.constraint(equalTo: view.leftAnchor)
        
        view.addConstraints([topConstraint, rightConstraint, bottomConstraint, leftConstraint])
        
        mapView = map
    }
    
    // MARK: - View manipulation methods
    
    private func releaseMap() {
        mapView?.removeFromSuperview()
        mapView = nil
    }
    
}

extension MapViewController: MapView {
    
    func displayLocation(from location: GeoLocation) {
        let delta = 0.01
        let coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        let coordinateSpan = MKCoordinateSpan.init(latitudeDelta: delta, longitudeDelta: delta)
        let mapRegion = MKCoordinateRegion(center: coordinate, span: coordinateSpan)
        let hotelAnnotation = MKPointAnnotation()
        
        hotelAnnotation.coordinate = coordinate
        
        mapView?.setRegion(mapRegion, animated: false)
        mapView?.addAnnotation(hotelAnnotation)
    }
    
}
