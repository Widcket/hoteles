//
//  MapPresenter.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation

final class MapPresenter {
    
    var view: MapView?
    var location: GeoLocation?
    
}

extension MapPresenter: MapEventsHandler {
    
    func viewWillAppear() {
        guard let view = view, let location = location else { return }
        
        view.displayLocation(from: location)
    }
    
}
