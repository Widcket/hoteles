//
//  MapScene.swift
//  Hoteles
//
//  Created by Rita Zerrizuela on 14/04/2019.
//  Copyright © 2019 Rita Zerrizuela. All rights reserved.
//

import Foundation
import UIKit
import Swinject

// MARK: - Scene protocols

protocol MapView: class {
    
    var eventsHandler: MapEventsHandler? { get set }
    
    func displayLocation(from location: GeoLocation)
    
}

protocol MapEventsHandler: class {
    
    var view: MapView? { get set }
    var location: GeoLocation? { get set }
    
    func viewWillAppear()
    
}

// MARK: - Scene

final class MapScene {
    
    private init() {}
    
    static func build(with container: Container, location: GeoLocation) -> UIViewController {
        guard let view = container.resolve(MapView.self),
            let presenter = container.resolve(MapEventsHandler.self) else {
                fatalError("Unable to resolve \(String(describing: self)) dependencies")
        }
        
        view.eventsHandler = presenter
        presenter.view = view
        presenter.location = location
        
        return view as! UIViewController
    }
    
    static func registerDependencies(in container: Container) {
        
        container.register(MapView.self) { resolver in
            return MapViewController()
        }
        
        container.register(MapEventsHandler.self) { resolver in
            return MapPresenter()
        }
    }
    
}
